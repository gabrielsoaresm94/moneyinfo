const modoDev = process.env.NODE_ENV !== 'production';
const webpack = require('webpack');
const miniCssExtractPlugin = require('mini-css-extract-plugin');
const uglifyJsPlugin = require('mini-css-extract-plugin');
//const optimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const htmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
    mode: modoDev ? 'development' : 'production',
    entry: './src/js/index.js',
    output: {
        filename: 'main.js',
        path: __dirname + '/public'
    },
    devServer: {
        contentBase: './public',
        port: 8080,
    },
    optimization: {
        minimizer: [
            new uglifyJsPlugin({
                cache: true,
                parallel: true
            }),
            //new optimizeCssAssetsPlugin({}),
        ]
    },
    plugins: [
        new miniCssExtractPlugin({
            filename: "style.css"
        }),
        new htmlWebpackPlugin({
            title: 'React + WebPack 4',
            template: './public/index.html',
      }),
    ],
    module: {
        rules: [{
            test: /\.(jsx|js)?$/,
            exclude: /node_modules/,
            use: {
                loader: 'babel-loader?cacheDirectory'
            }
        }, {
            test: /\.s?[ac]ss$/,
            use: [
                //'style-loader', // adiciona css a dom injetando a tag <style>
                miniCssExtractPlugin.loader,
                'css-loader', // interpreta @import , url() ...
                'sass-loader'
            ]
        }, {
            test: /\.(png|svg|jpg|jpeg|gif)$/,
            use: ['file-loader']
        }]
    }
}
