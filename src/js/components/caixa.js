import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Select from '@material-ui/core/Select';
import { FormControl, InputLabel, MenuItem, TextField } from '@material-ui/core';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

//colocar data
//botão com o valor total

const CustomTableCell = withStyles(theme => ({
  head: {
    backgroundColor: theme.palette.common.white, //black || white
    color: theme.palette.common.black,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const styles = theme => ({
    root: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    /*margin: {
    margin: theme.spacing.unit,
    },
    cssLabel: {
    '&$cssFocused': {
      color: 'purple',
    },
  },
  cssFocused: {},
  cssUnderline: {
    '&:after': {
      borderBottomColor: 'purple',
    },
  },
  cssOutlinedInput: {
    '&$cssFocused $notchedOutline': {
      borderColor: 'purple',
    },
  },
  notchedOutline: {},
  bootstrapRoot: {
    'label + &': {
      marginTop: theme.spacing.unit * 3,
    },
  },
  bootstrapInput: {
    borderRadius: 4,
    position: 'relative',
    backgroundColor: theme.palette.common.white,
    border: '1px solid #ced4da',
    fontSize: 16,
    width: 'auto',
    padding: '10px 12px',
    transition: theme.transitions.create(['border-color', 'box-shadow']),
    // Use the system font instead of the default Roboto font.
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
    '&:focus': {
      borderRadius: 4,
      borderColor: '#80bdff',
      boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
    },
  },
  bootstrapFormLabel: {
    fontSize: 18,
  },*/
    selectEmpty: {
      marginTop: theme.spacing.unit * 2,
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 200,
    },
    table: {
        //minWidth: 700,
    },
    row: {
        '&:nth-of-type(odd)': {
             backgroundColor: theme.palette.background.default,
         },
    },
  });

let id = 0;
function createData(name, calories, fat, carbs, protein) {
  id += 1;
  return { id, name, calories, fat, carbs, protein };
}

const rows = [
  createData('Gabriel', 'pagar fulano', - 120),
  createData('Isys', 'pagamento de projeto', 250),
];


class Caixa extends Component {

    state = {
        maybe: '',
    }

    render() {
        const { classes } = this.props;
        return(
	<div >
            <form style={{marginLeft: -10, marginRight: -10}}>
                <FormControl fullWidth >
                    <InputLabel classes={{
            root: classes.cssLabel,
            focused: classes.cssFocused,
          }}>Operadores</InputLabel>
                    <Select value={this.state.maybe}>
                        <MenuItem >-</MenuItem>
                        <MenuItem >+</MenuItem>
                    </Select>
                </FormControl>
		<TextField
		   fullWidth
                   id="standard-search"
                   label="Quantia"
                   type="number"
                   margin="normal"
                />
                <TextField
		   fullWidth
                   id="standard-search"
                   label="Quem?"
                   margin="normal"
                />
		<TextField
          	   id="standard-search"
          	   label="Pra quê?"
          	   //placeholder="Placeholder"
          	   //helperText="Full width!"
          	   fullWidth
          	   margin="normal"
          	   
        	/>
            </form>
	    <div style={{marginTop: 20}}>
	    <Paper className={classes.root} style={{marginLeft: -10, marginRight: -10}}>
              <Table className={classes.table}>
                <TableHead>
                  <TableRow>
                    <CustomTableCell>Quem</CustomTableCell>
                    <CustomTableCell align="left">Motivo</CustomTableCell>
                    <CustomTableCell align="right">Quantia</CustomTableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                 {rows.map(row => (
                   <TableRow className={classes.row} key={row.id}>
                     <CustomTableCell component="th" scope="row">
                       {row.name}
                     </CustomTableCell>
                     <CustomTableCell align="left">{row.calories}</CustomTableCell>
                     <CustomTableCell align="right">{row.fat}</CustomTableCell>
                   </TableRow>
                 ))}
                </TableBody>
              </Table>
            </Paper>
	    </div>
	</div>
        );
    }
}

Caixa.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Caixa);

//colocar data da retirada!
//complicado, acompanhar o styles dos exemplos para editar os styles desses components
